/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanisata.javaswingproject;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Friend implements Serializable{
    private String name;
    private int age;
    private String gender;
    private String descri;

    public Friend(String name, int age, String gender, String descri) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.descri = descri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    @Override
    public String toString() {
        return "Friend{" + "name = " + name + ", age = " + age + ", gender = " + gender + ", descri = " + descri + '}';
    }
    
}
